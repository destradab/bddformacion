package com.choucair.formacion.definition;

import com.choucair.formacion.steps.AgregaPacienteSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AgregarPacienteDefinition {

	@Steps
	AgregaPacienteSteps	agregaPacienteSteps;
	
	@Given("^que Carlos necesita registrar un nuevo paciente$")
	public void que_Carlos_necesita_registrar_un_nuevo_paciente() {
		agregaPacienteSteps.abreUrl();
		agregaPacienteSteps.ingresaModulo();
	}

	@When("^el realiza el registro del paciente en mismo en el aplicativo de Administración de Hospitales$")
	public void el_realiza_el_registro_del_paciente_en_mismo_en_el_aplicativo_de_Administración_de_Hospitales() {
		agregaPacienteSteps.registraPaciente();
	}

	@Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente para el paciente$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente_para_el_paciente() {

	}

	
}
