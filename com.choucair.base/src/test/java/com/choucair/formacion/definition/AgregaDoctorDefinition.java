package com.choucair.formacion.definition;

import com.choucair.formacion.steps.AgregaDoctorSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AgregaDoctorDefinition {
	
	@Steps
	AgregaDoctorSteps	agregaDoctorSteps;
	
	@Given("^que Carlos necesita registrar un nuevo doctor$")
	public void que_Carlos_necesita_registrar_un_nuevo_doctor() {
		agregaDoctorSteps.abreUrl();
		agregaDoctorSteps.abrirModulo();
	}

	@When("^el realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void el_realiza_el_registro_del_mismo_en_el_aplicativo_de_Administración_de_Hospitales() {
		agregaDoctorSteps.registraDoctor();
	}

	@Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente() {

	}
	
}
