package com.choucair.formacion.definition;

import com.choucair.formacion.steps.AgregaCitaSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AgregaCitaDefinition {
	
	@Steps
	AgregaCitaSteps	agregaCitaSteps;
	
	@Given("^que Carlos necesita asistir al medico$")
	public void que_Carlos_necesita_asistir_al_medico() throws Throwable {
		agregaCitaSteps.abreUrl();
		agregaCitaSteps.ingresaModulo();
	}

	@When("^el realiza el agendamiento de una Cita$")
	public void el_realiza_el_agendamiento_de_una_Cita() throws Throwable {
		agregaCitaSteps.registraCita();
	}

	@Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente para la cita$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente_para_la_cita() throws Throwable {
	    
	}


}
