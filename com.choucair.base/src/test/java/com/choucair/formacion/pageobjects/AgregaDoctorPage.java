package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AgregaDoctorPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[1]")
	private WebElementFacade menuAgregarDr;

	public void abreModuloAgregarDoctor() {
		menuAgregarDr.click();
	}

	public void registraDoctor(String nombre, String apellido, String telefono2, String tipoDocumento,String documento) {
		findBy("//INPUT[@id='name']").sendKeys(nombre);
		findBy("//INPUT[@id='last_name']").sendKeys(apellido);
		findBy("//INPUT[@id='telephone']").sendKeys(telefono2);
		findBy("//SELECT[@id='identification_type']/option[2]").click();
		findBy("//INPUT[@id='identification']").sendKeys(documento);
		findBy("//A[@onclick='submitForm()'][text()='Guardar']").click();
	}

}
