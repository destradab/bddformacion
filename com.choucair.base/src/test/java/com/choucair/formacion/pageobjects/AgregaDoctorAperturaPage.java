package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class AgregaDoctorAperturaPage extends PageObject{

	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]")
	private WebElementFacade menuAgregarPaciente;

	public void abreModuloAgregarDoctor() {
		// TODO Auto-generated method stub
		menuAgregarPaciente.click();
	}

}
