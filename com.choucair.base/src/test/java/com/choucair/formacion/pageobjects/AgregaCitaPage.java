package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AgregaCitaPage extends PageObject{

	@FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[6]")
	private WebElementFacade menuAgregarCita;
	

	public void abreModuloAgregarCita() {
		menuAgregarCita.click();
	}

	public void registraCita(String diaCita, String documentoPaciente, String documentoDoctor, String observacion) {
		// TODO Auto-generated method stub
		findBy("//INPUT[@id='datepicker']").sendKeys(diaCita);
		findBy("(//INPUT[@type='text'])[2]").sendKeys(documentoPaciente);
		findBy("(//INPUT[@type='text'])[3]").sendKeys(documentoDoctor);
		findBy("//TEXTAREA[@class='form-control']").sendKeys(observacion);
		findBy("//A[@onclick='submitForm()'][text()='Guardar']").click();
		waitABit(650000);
	}

}
