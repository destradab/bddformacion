package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AgregaPacientePage extends PageObject{

	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]")
	private WebElementFacade menuAgregarPaciente;
	
	public void abreModuloAgregarDoctor() {
		menuAgregarPaciente.click();
	}

	public void registraPaciente(String nombre, String apellido, String telefono, String tipoDocumento,String documento, boolean prepagada) {
		// TODO Auto-generated method stub
		findBy("(//INPUT[@type='text'])[1]").sendKeys(nombre);
		findBy("(//INPUT[@type='text'])[2]").sendKeys(apellido);
		findBy("(//INPUT[@type='text'])[3]").sendKeys(telefono);
		findBy("//SELECT[@class='form-control']/option[2]").click();
		findBy("(//INPUT[@type='text'])[4]").sendKeys(documento);
		if (prepagada == true ) {
			findBy("//INPUT[@type='checkbox']").click();
		}else {
			
		}
		findBy("//A[@onclick='submitForm()'][text()='Guardar']").click();
		waitABit(650000);
		
	}

}
