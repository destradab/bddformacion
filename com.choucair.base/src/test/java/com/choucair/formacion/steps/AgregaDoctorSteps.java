package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.AgregaDoctorAperturaPage;
import com.choucair.formacion.pageobjects.AgregaDoctorPage;

import net.thucydides.core.annotations.Step;

public class AgregaDoctorSteps {

	AgregaDoctorAperturaPage AgregaDrMasivoAperturaPage;
	AgregaDoctorPage agregaDrMasivoPage;

	@Step
	public void abreUrl() {
		AgregaDrMasivoAperturaPage.open();
	}

	@Step
	public void abrirModulo() {
		agregaDrMasivoPage.abreModuloAgregarDoctor();
	}

	public void registraDoctor() {
		String nombre, apellido, telefono, tipoDocumento, documento;
		nombre = "Diego1";
		apellido = "Estrada1";
		telefono = "23456781";
		tipoDocumento = "Cédula de ciudadanía";
		documento = "10184057552";
		agregaDrMasivoPage.registraDoctor(nombre, apellido, telefono, tipoDocumento, documento);
	}

}