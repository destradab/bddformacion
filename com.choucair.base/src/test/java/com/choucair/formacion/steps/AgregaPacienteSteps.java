package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.AgregaDoctorAperturaPage;
import com.choucair.formacion.pageobjects.AgregaPacientePage;

import net.thucydides.core.annotations.Step;

public class AgregaPacienteSteps {
	
	AgregaDoctorAperturaPage AgregaDrMasivoAperturaPage;
	AgregaPacientePage agregaPacientePage;
	
	@Step
	public void abreUrl() {
		AgregaDrMasivoAperturaPage.open();
	}

	@Step
	public void ingresaModulo() {
		// TODO Auto-generated method stub
		agregaPacientePage.abreModuloAgregarDoctor();
	}

	public void registraPaciente() {
		String nombre, apellido, telefono, tipoDocumento, documento;
		boolean prepagada;
		nombre = "Pepe";
		apellido = "Estrada";
		telefono = "2345678";
		tipoDocumento = "Cédula de ciudadanía";
		documento = "10184057551";
		prepagada = true;
		agregaPacientePage.registraPaciente(nombre, apellido, telefono, tipoDocumento, documento,prepagada);
	}

}