package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.AgregaCitaPage;
import com.choucair.formacion.pageobjects.AgregaDoctorAperturaPage;

public class AgregaCitaSteps {

	AgregaDoctorAperturaPage AgregaDrMasivoAperturaPage;
	AgregaCitaPage agregaCitaPage;
	
	public void abreUrl() {
		// TODO Auto-generated method stub
		AgregaDrMasivoAperturaPage.open();
	}

	public void ingresaModulo() {
		// TODO Auto-generated method stub
		agregaCitaPage.abreModuloAgregarCita();
	}

	public void registraCita() {
		// TODO Auto-generated method stub
		String diaCita, documentoPaciente, documentoDoctor, observacion;
		diaCita = "03/01/2019";
		documentoPaciente = "10184057551";
		documentoDoctor = "10184057552";
		observacion = "El paciente es enviado por Diego Estrada";

		agregaCitaPage.registraCita(diaCita, documentoPaciente, documentoDoctor, observacion);
	}

}
