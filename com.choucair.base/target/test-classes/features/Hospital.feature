#Author: your.email@your.domain.com

@completo
Feature: hospital
  
  @tag1
  Scenario: Agrega Doctor
    Given que Carlos necesita registrar un nuevo doctor
    When el realiza el registro del mismo en el aplicativo de Administración de Hospitales
    Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente

	@tag2
	Scenario: Realizar el Registro de un Paciente
	Given que Carlos necesita registrar un nuevo paciente
	When el realiza el registro del paciente en mismo en el aplicativo de Administración de Hospitales
	Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente para el paciente
      
	@tag3
	Scenario: Realizar el Agendamiento de una Cita
	Given que Carlos necesita asistir al medico
	When el realiza el agendamiento de una Cita
	Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente para la cita
	      