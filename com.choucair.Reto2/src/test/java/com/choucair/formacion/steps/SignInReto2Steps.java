package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.OpenPageChallenge2;
import com.choucair.formacion.pageobjects.SignInReto2Page;

import net.thucydides.core.annotations.Step;

public class SignInReto2Steps {

	OpenPageChallenge2	openPageChallenge2;
	SignInReto2Page signInReto2Page;

	@Step
	public void openUrl() {
		openPageChallenge2.open();
	}
	
	@Step
	public void acceptCookie() {
		signInReto2Page.acceptCookie();
	}
	
	@Step
	public void verifyUrlLoad() {
		signInReto2Page.checkIntro();
	}

}
