package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.FillFormReto2Page;

import net.thucydides.core.annotations.Step;

public class FillFormReto2Steps {
	
	FillFormReto2Page	fillFormReto2Page;

	@Step
	public void fillData(List<List<String>> data, int i) {
		fillFormReto2Page.name(data.get(i).get(0).trim());
		fillFormReto2Page.LastName(data.get(i).get(1).trim());
		fillFormReto2Page.country(data.get(i).get(2).trim());
		fillFormReto2Page.city(data.get(i).get(3).trim());
		fillFormReto2Page.phone(data.get(i).get(4).trim());
		fillFormReto2Page.mail(data.get(i).get(5).trim());
		fillFormReto2Page.languaje(data.get(i).get(6).trim());
		fillFormReto2Page.Platforma(data.get(i).get(7).trim());
		fillFormReto2Page.account(data.get(i).get(8).trim());
		fillFormReto2Page.coin(data.get(i).get(9).trim());
		fillFormReto2Page.leverage(data.get(i).get(10).trim());
		fillFormReto2Page.Quantity(data.get(i).get(11).trim());
		fillFormReto2Page.password1(data.get(i).get(12).trim());
		fillFormReto2Page.password2(data.get(i).get(13).trim());
	}

	public void checkButton() {
		fillFormReto2Page.checkButton();
	}

	public void openAccount() {
		fillFormReto2Page.openAccount();
	}

	public void verifyBanner() {
		try {
			fillFormReto2Page.verifyBanner();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
