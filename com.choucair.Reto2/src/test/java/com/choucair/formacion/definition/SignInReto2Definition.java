package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.FillFormReto2Steps;
import com.choucair.formacion.steps.SignInReto2Steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SignInReto2Definition {

	@Steps
	SignInReto2Steps signInReto2Steps;

	@Steps
	FillFormReto2Steps fillFormReto2Steps;

	@Given("^That user opens the URL$")
	public void that_user_open_the_URL() {
		signInReto2Steps.openUrl();
	}

	@Given("^Accept cookie$")
	public void accept_cookie() {
		signInReto2Steps.acceptCookie();
	}

	@Given("^checks that website complete the load process$")
	public void checks_that_website_complete_the_load_process() {
		signInReto2Steps.verifyUrlLoad();
	}

	@When("^user fill all data With <name> <LastName> <country> <city> <phone> <mail> <languaje> <Platforma> <account> <coin> <leverage> <Quantity> <password(\\d+)> <password(\\d+)>$")
	public void user_fill_all_data_With_name_LastName_country_city_phone_mail_languaje_Platforma_account_coin_leverage_Quantity_password_password(
			int arg1, int arg2, DataTable table1) {
		List<List<String>> data = table1.raw();
		for (int i = 1; i < data.size(); i++) {
			fillFormReto2Steps.fillData(data, i);
		}
		System.out.println("Out Cicle 1");
	}

	@When("^click the buton of open a demo account$")
	public void now_check_the_checkbox_and_click_the_buton_of_open_a_demo_account() {
		fillFormReto2Steps.openAccount();
	}
	
	@When("^verifyTheSuccesfullAccountProcess$")
	public void verifyTheSuccesfullAccountProcess() {
		fillFormReto2Steps.verifyBanner();
	}

}
