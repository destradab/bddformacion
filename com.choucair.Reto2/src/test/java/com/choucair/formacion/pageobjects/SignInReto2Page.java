package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SignInReto2Page extends PageObject{
	
	//Intro Label
	@FindBy(xpath = "/html/body/div[2]/div/div/div[1]/div[2]/div[2]/div/button")
	public WebElementFacade acceptCookie;
	
	//Intro Label
	@FindBy(xpath = "//*[@id=\'form\']/div[1]/div[1]/div/div[1]/h1")
	public WebElementFacade introForm;

	public void checkIntro() {
		String labelIntro = "Registro de Cuenta Demo";
		String textVerify = introForm.getText();
		assertThat(textVerify, containsString(labelIntro));
	}

	public void acceptCookie() {
		acceptCookie.click();
	}

}
