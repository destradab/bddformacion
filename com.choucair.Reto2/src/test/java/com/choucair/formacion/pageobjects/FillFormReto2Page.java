package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class FillFormReto2Page extends PageObject {

	@FindBy(xpath = "//*[@id='demo-form']/div[3]/div[14]/div/div/div/label")
	public WebElementFacade check;
	
	public void name(String trim) {
		findBy("//*[@id=\'first_name\']").sendKeys(trim);
	}

	public void LastName(String trim) {
		findBy("//*[@id=\'last_name\']").sendKeys(trim);
	}

	public void country(String trim) {
		findBy("//*[@id=\'country\']").sendKeys(trim);
	}

	public void city(String trim) {
		findBy("//*[@id=\'city\']").sendKeys(trim);
	}

	public void phone(String trim) {
		findBy("//*[@id=\'phone_number\']").sendKeys(trim);
	}

	public void mail(String trim) {
		findBy("//*[@id=\'email\']").sendKeys(trim);
	}

	public void languaje(String trim) {
		findBy("//*[@id=\'preferred_language\']").sendKeys(trim);
	}

	public void Platforma(String trim) {
		findBy("//*[@id=\'trading_platform_type\']").sendKeys(trim);
	}

	public void account(String trim) {
		findBy("//*[@id=\'account_type\']").sendKeys(trim);
	}

	public void coin(String trim) {
		findBy("//*[@id=\'account_currency\']").sendKeys(trim);
	}

	public void leverage(String trim) {
		findBy("//*[@id=\'account_leverage\']").sendKeys(trim);
	}

	public void Quantity(String trim) {
		findBy("//*[@id=\'investment_amount\']").sendKeys(trim);
	}

	public void password1(String trim) {
		findBy("//*[@id=\'account_password\']").sendKeys(trim);
	}

	public void password2(String trim) {
		findBy("//*[@id=\'account_password_confirmation\']").sendKeys(trim);
	}

	public void checkButton() {
		findBy("//*[@id='demo-form']/div[3]/div[14]/div/div/div/label").click();
	}
	
	public void openAccount() {
		findBy("//*[@id=\'submit-btn\']").submit();
	}

	public void verifyBanner() {
		findBy("//*[@id=\'top\']/div[1]/div/div/div[1]/div[1]/div/div[2]").isVisible();
	}
}
