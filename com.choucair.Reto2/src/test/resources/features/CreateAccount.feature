#Author: destradab@choucairtesting.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@createAccounts
Feature: Feature used to create a new account

  @SignIn
  Scenario: SignIn
    Given That user opens the URL
    And Accept cookie 
    And checks that website complete the load process
    When user fill all data With <name> <LastName> <country> <city> <phone> <mail> <languaje> <Platforma> <account> <coin> <leverage> <Quantity> <password1> <password2>
    | name | LastName | country | city | phone | mail | languaje | Platforma | account | coin | leverage | Quantity | password1 | password2 |
    | diego1 | Alejandro1 | Colombia | Bogota | 23456781 | mymail1@mail.com | Inglés | MT5 (forex, CFDs sobre acciones, índices bursátiles, metales y energías) | Standard (1 lote=100,000) | USD | 1:100 | 100.000 | Clave1Clave1 | Clave1Clave1 |
    #And now check the checkbox and click the buton of open a demo account
    And click the buton of open a demo account
    And verifyTheSuccesfullAccountProcess
    
    @Scenary
 		Scenario: SignIn
    Given That user opens the URL
    And Accept cookie 
    And checks that website complete the load process
    When user fill all data With <name> <LastName> <country> <city> <phone> <mail> <languaje> <Platforma> <account> <coin> <leverage> <Quantity> <password1> <password2>
    | name | LastName | country | city | phone | mail | languaje | Platforma | account | coin | leverage | Quantity | password1 | password2 |
    | diego1 | Alejandro1 | Colombia | Bogota | 23456781 | mymail1@mail.com | Inglés | MT5 (forex, CFDs sobre acciones, índices bursátiles, metales y energías) | Standard (1 lote=100,000) | USD | 1:100 | 100.000 | Clave1Clave1 | Clave1Clave1 |
    #And now check the checkbox and click the buton of open a demo account
    And click the buton of open a demo account
    And verifyTheSuccesfullAccountProcess